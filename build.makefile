# -*-Makefile-*- for maintenance jobs.

topdir := $(shell pwd)

TARGET := $(shell arch)-linux

VERSION := $(shell ls -1 SOURCES/xllmnrd-*.tar.gz | \
sed -e 's,.*/xllmnrd-\(.*\)\.tar\.gz,\1,' -e '2q1')
RELEASE := 1

RPMBUILD = rpmbuild -D '_topdir $(topdir)'
RPMBUILDFLAGS = --nodeps -D '_isa %{nil}'

build: SPECS/xllmnrd.spec
	rm -fr RPMS SRPMS
	$(RPMBUILD) $(RPMBUILDFLAGS) --target=$(TARGET) \
	  --clean -ba SPECS/xllmnrd.spec

SPECS/xllmnrd.spec: update-spec
update-spec: SPECS/xllmnrd.spec.in build.makefile
	@rm -f SPECS/xllmnrd.spec-t SPECS/xllmnrd.spec
	sed -e 's/[@]VERSION[@]/$(VERSION)/g' \
	  -e 's/[@]RELEASE[@]/$(RELEASE)/g' \
	  < SPECS/xllmnrd.spec.in > SPECS/xllmnrd.spec-t
	if test -s SPECS/xllmnrd.spec-t && \
	    ! cmp -s SPECS/xllmnrd.spec-t SPECS/xllmnrd.spec; then \
	  mv -f SPECS/xllmnrd.spec-t SPECS/xllmnrd.spec; \
	else \
	  rm -f SPECS/xllmnrd.spec-t; \
	fi

.PHONY: build
